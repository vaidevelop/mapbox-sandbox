
mapboxgl.accessToken = 'pk.eyJ1IjoiamVhbnZpZXV4IiwiYSI6Iksxc0Z3dTAifQ.enIr_jXRCpLAfZQa8rmdjA';

var map = new mapboxgl.Map({
  container: 'map',
  style: 'mapbox://styles/mapbox/light-v9',
  zoom: 12,
	center: [-87.622088, 41.878781]
});

map.on('load', function() {

  map.addLayer({
    "id": "my_id",
    "type": "line",
    "source": {
      "type": "vector",
      "tiles": ["http://example.com/{z}/{x}/{y}.mvt"]
    },
    "source-layer": "my_id",
    "layout": {
      "line-cap": "round",
      "line-join": "round"
    },
    "paint": {
      "line-opacity": 0.6,
      "line-color": "rgb(53, 175, 109)",
      "line-width": 2
    }
  });
});
